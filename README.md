# Ayudante Bibliografía

Es un pequeño script diseñado para suplir la necesidad de crear bibliografía en formato APA para documentos en formato PDF y videos.

No se intenta reinventar la rueda para los libros y páginas web ya que para eso utilizo [bibme.org](bibme.org), específicamente la sección de "citation".

Dado que [bibme.org](bibme.org) dá la bibliografía con algunas palabras en inglés, el script proporciona la funcionalidad de traducir dichas palabras.

# Consideraciones
* **Este script esta en desarrollo**, aunque no de manera dedicada
* El formato APA **puede no ser preciso ni adecuado para tu caso de uso**, simplemente solventa mi problema.
