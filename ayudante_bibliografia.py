#!/usr/env python

import sys


def capturar_informacion(tipo_bib: str) -> dict:
    informacion: dict = {}

    print("Autor")
    try:
        informacion['nombre_autor'] = str(input("\tNombre: ")[0])
    except:
        informacion['nombre_autor'] = ""

    informacion['apellido_autor'] = str(input("\tApellidos: "))

    if tipo_bib == "video":
        informacion['canal_video'] = str(input("\tNombre del Canal: "))

    print("\nObra")
    informacion['titulo'] = str(input("\tTitulo: "))
    informacion['fecha_publicacion'] = str(input("\tFecha publicación (año, mes dia): "))
    informacion['url'] = str(input("\tURL: "))

    if tipo_bib == "pdf":
        informacion['lugar_publicacion'] = str(input("\tLugar publicación: "))

    return informacion


def citar_pdf(informacion: dict) -> str:
    """
    Referencia: https://bibliografias.org/como-citar-archivos-pdf-en-el-estilo-apa/
    """
    bib = informacion["apellido_autor"] + ", " + informacion["nombre_autor"] + \
        "(" + informacion["fecha_publicacion"] + "). " + informacion["titulo"] + \
        " [Archivo PDF]. " + informacion['lugar_publicacion'] + ". " + \
        "Recuperado de " + informacion["url"]

    return bib


def citar_video_youtube(informacion: dict) -> str:
    # Apellidos, Iniciales nombre. [Nombre del Canal]. (Año, mes día). Titulo del video [Video]. Youtube. URL

    bib = informacion["apellido_autor"] + ", " + informacion["nombre_autor"] + \
          ". [" + informacion["canal_video"] + "]. (" + \
          informacion["fecha_publicacion"] + "). " + informacion["titulo"] + \
          " [Video]. Youtube. " + informacion["url"]

    return bib


def traducir_bibme(apa_bib_en_ingles: str) -> str:
    meses = {'January': 'Enero', 'February': 'Febrero', 'March': 'Marzo',
             'April': 'Abril', 'May': 'Mayo', 'June': 'Junio', 'July': 'Julio',
             'August': 'Agosto', 'September': 'Septiembre', 'October': 'Octubre',
             'November': 'Noviembre', 'December': 'Diciembre'}

    # TODO: 'from' e la URL de la bibliografia son un corner case potencial
    palabras_clave = {'Retrieved': 'Recuperado', 'from': 'de'}

    for k, v in meses.items():
        for word in apa_bib_en_ingles.split():
            # 'k + ")."' caso especial
            if word == k or word == k + ").":
                apa_bib_en_ingles = apa_bib_en_ingles.replace(k, v)

    for k, v in palabras_clave.items():
        for word in apa_bib_en_ingles.split():
            if word == k:
                apa_bib_en_ingles = apa_bib_en_ingles.replace(k, v)

    return apa_bib_en_ingles

def menu(listaOpciones):
    '''
    Recibe una lista con las opciones disponibles para el menu.
    Asigna un numero natural a cada item en la lista
    retorna la opcion seleccionada como un entero (natural)
    '''
    for elemento in range(len(listaOpciones)):
        indice = elemento + 1
        print("{}. {}".format(indice, listaOpciones[elemento]))

    """
    si la opcion seleccionada no es la adecuada se devuelve el valor -1
    puede considerarse como error
    """
    try:
        opcion = int(input(">? "))
    except:
        opcion = -1
    return opcion


if __name__ == "__main__":
    opcionesMenu = ["PDF", "Video Youtube", "Traducir APA en inglés", "Salir"]

    while True:
        bib = ""
        mostrar_bib: bool = True

        print("Citar:")
        opcion = menu(opcionesMenu)

        if opcion == 1:
            bib = citar_pdf(capturar_informacion("pdf"))

        elif opcion == 2:
            bib = citar_video_youtube(capturar_informacion("video"))

        elif opcion == 3:
            apa_en_ingles = str(input("bibliografia en inglés: "))
            bib = traducir_bibme(apa_en_ingles)

        elif opcion == 4:
            sys.exit()

        else:
            print("opcion no reconocida\n")
            mostrar_bib: bool = False

        if mostrar_bib:
            print(f"\nbibliografía:\n{bib}\n")
